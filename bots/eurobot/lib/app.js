// Discord consts
const Discord = require('discord.js');
const RichEmbed = Discord.RichEmbed;

export default class App {

	constructor(options){

        this.opt = {
            db:require('../options/db'),
            discord:require('../options/discord'),
            whitelist:require('../options/whitelist'),
            responses:require('../options/responses'),

            calendar:require('../options/calendar/options')

        };

        // global designations
        this.Intervals = [];

        // mysql pool
        if(this.opt.db.enabled) {
            const mysql = require('mysql');
            this.pool = mysql.createPool(this.opt.db.settings);    
        }
        
        // keyword timeouts
        this.kwdTimeout = false;
        this.kwdTimer = true;

        //
        // Custom Bot Includes and Constructors
        //

        // Google API
        const {google} = require('googleapis');;
        const privatekey = require("../options/calendar/Eurobot-Calendar-1c54f0456b16.json");

        //Google Calendar API
        this.calendar = google.calendar('v3');

        // configure a Google JWT auth client
        this.jwtClient = new google.auth.JWT(

            privatekey.client_email,
            null,
            privatekey.private_key,
            ['https://www.googleapis.com/auth/calendar']

        );

        //Google API authenticate request
        this.jwtClient.authorize(function (err, tokens) {

            if (err) {
                throw err;
            } else {
                console.log("Successfully connected to Google API");
            }

        });

        // Suggestions
        this.suggestions = {};
        this.suggestions.status = {
            PENDING: 'PENDING',
            VOTED: 'VOTED',
            IMPLEMENTED: 'IMPLEMENTED',
        };
        this.suggestions.list = './data/suggestionsList.json';

    }

    ban(member, author){

        return new Promise((resolve,reject)=>{

            let name;
            (member.user && member.user.username)? name = member.user.username : false;

            if(name) {
                
                let embed = new RichEmbed()
                    .setTitle('Ban by '+ author)
                    .setDescription("Banned " + name)
                    .setColor(0x003399);
        
                member.ban()
                    .then(() => {

                        resolve(embed);

                    })
                    .catch(() => {

                        embed.setTitle('ERROR: attempted Ban by '+ author)
                            .setDescription("NOT Banned " + name);

                        resolve(embed);

                    });
                
            } else {

                reject();

            }

        });

    }

    // Kick user
    kick(member, author){

        return new Promise((resolve,reject)=>{

            let name;
            (member.user && member.user.username)? name = member.user.username : false;

            if(name) {

                let embed = new RichEmbed()
                    .setTitle('Kick by '+ author)
                    .setDescription("Kicked " + name)
                    .setColor(0x003399);

                member.kick()
                    .then(() => {

                        resolve(embed);

                    })
                    .catch(() => {

                        embed.setTitle('ERROR: attempted Kick by '+ author)
                            .setDescription("NOT Kicked " + name);

                        resolve(embed);

                    });

            } else {

                reject();

            }
            
        });
    }
    
    remove(member, author, message){

        return new Promise((resolve,reject)=>{

            let embed = new RichEmbed()
                .setTitle('Delete by '+ member)
                .setDescription("Deleted " + author + ": " + message.content)
                .setColor(0x003399);

            message.delete()
                .then(() =>{

                    resolve(embed)

                })
                .catch(() => {

                    embed.setTitle('ERROR: attempted Delete by '+ member)
                        .setDescription("NOT Deleted " + author + ": " + message.content);
                    
                    resolve(embed);

                });

        });          
        
    }

	// // Execute MySQL query
	q(sql,arrayOrObj){

        try {

    		return new Promise((resolve,reject)=> {

                if(this.pool) {
                    
                    this.pool.getConnection(function(err, connection) {

                        if(err) throw err;

                        connection.query(sql,arrayOrObj,(error, results)=>{

                            connection.release();

                            // Handle error after the release.
                            if (error) throw error;

                            resolve(results);

                    });


                    });
    
                } else {

                    reject('No pool');
                    
                }

            });

		} catch(e) {

           throw e;

        }

	}

    // change date
    setCalendarDate(DateObj) {
        let mm = DateObj.getMonth() + 1,
            dd = DateObj.getDate();

        let date = [
            DateObj.getFullYear() + '-',
            (mm>9 ? '' : '0') + mm + '-',
            (dd>9 ? '' : '0') + dd
        ].join('');
                
        return date + ' ' + [
                (DateObj.getHours() > 9 ? '' : '0') + DateObj.getHours() +':',
                (DateObj.getMinutes() > 9 ? '' : '0') + DateObj.getMinutes()
            ].join(''); 
    }

    // getMsgOpts: parse message options (spaces, ignore commas)
    getMsgOpts(msg,opts) {

        let list = msg.split(' ');
        let rtn = [];
        if(list.length>1)
        for(let i=1,c=list.length;i<c;i++){

            let dataLet = list[i]
            dataLet = dataLet.split(',').join('').trim();
            if(opts && opts.capitalize) {
                dataLet = dataLet.toLowerCase();
                dataLet = dataLet.charAt(0).toUpperCase() + dataLet.slice(1);
            }
            rtn.push(dataLet);

        }
        return rtn;

    }

    // retrieve calendar items from google
    getGoogleCalendarItems(opts,to,from) {

        if(!from) from = new Date(new Date().setHours(0,0,0,0));
        if(!to) to = new Date(from.getTime() + (86400000 -1));;

        return new Promise((resolve,reject)=>{

            this.calendar.events.list({
            
                auth: this.jwtClient,
                calendarId: opts.calendarID,
                timeMin:from,
                timeMax:to,
                singleEvents:true,
                orderBy:'startTime'
                
            }, function (err, response) {
        
                if (err) {

                    console.log('The Google Calendar API returned an error: ' + err);
                    reject(err);
                
                } else {

                    if(response && response.data && response.data.items) {
        
                        resolve(response.data.items);
        
                    } else {
        
                        console.log('The Google Calendar API returned an incompatible object');
                        reject();
        
                    }
        
                }

            });

        });

    }

    // convert items to calendar
    formatCalendar(items) {
        
        let rtn = '';

        items.forEach(item=>{

            let description = '';
            let dateString = '';

            if(item.start.date && item.end.date) {
                dateString = `${item.start.date} - ${item.end.date}`;
            }
            if(item.start.dateTime && item.end.dateTime) {

                dateString = this.setCalendarDate(new Date(item.start.dateTime));

                if((new Date(item.end.dateTime).getTime()) - (new Date(item.start.dateTime).getTime()) > 86400000) {
                    dateString += (' - ' + (this.setCalendarDate(new Date(item.end.dateTime))));
                } 

            }

            if(item.description) description = `${item.description}\n`;

            rtn += `**${item.summary}**\n${dateString}\n${description}\n`;

        });

        return rtn;

    }

    // // does any string start with any element in an array?
    matchStringStartToArray(string,array) {

        let match = false;
        array.forEach(el=>{
            if(string.toLowerCase().startsWith(el)) match = true;
        });
        return match;

    }

    // get the emoji from the guild emojis
    parseGuildEmoji(emoji,guildEmojis,returnsObjBool) {

        let eData = guildEmojis.find(e=>e.name === emoji);
        if(eData) {
            if(!returnsObjBool) return `<:${eData.name}:${eData.id}>`;
            return eData;
        }
        return false;

    }

    // find keywords in sentence
    parseSentenceForKeywords(string) {

        let rtn = false;

        for(let i in this.opt.responses.mentionKeywords) {

            if(string.includes(i)) {
            
                rtn = {type:i};
            
            }
            this.opt.responses.mentionKeywords[i].forEach(kwd=>{

                if(string.includes(kwd)) rtn = {type:i,keyword:kwd};

            });

        }

        return rtn;

    }

    // get mentions from discord message content
    getMentions(str){

        let msgArray = str.split(/ +/);
        let matches = [];
        
        if(msgArray.length < 1) return matches;

        msgArray.forEach(cell=>{
            let match = cell.match(/^<@!?(\d+)>$/);
            if(match) matches.push(match[1]);
        });

        return matches;

    }

    // Toggle role Obj for GuildMember Obj
    toggleRole(member,role) {

        if(member.roles) {

            let UserRole = member.roles.find(r=>r.id === role.id);
            
            if(!UserRole) {
                
                member.addRole(role.id)
                return true;

            } else {

                member.removeRole(role.id);
                return false;

            }

        } else {

            member.addRole(role.id);
            return true;

        }

    }

    // set global timeout on keywords for responses
    kwdTimerInit() {

        if(!this.kwdTimeout) {

            this.kwdTimeout = true;
            this.kwdTimer = setTimeout(()=>{
                this.kwdTimeout = false;
            },3000);
        
        }

    }

};